#!/usr/bin/env python

from __future__ import print_function, division

# Source of team ids is at the bottom of this page:
# https://github.com/seemethere/nba_py/wiki/stats.nba.com-Endpoint-Documentation
name_dict = {
    "hawks": 1610612737,
    "celtics": 1610612738,
    "nets": 1610612751,
    "hornets": 1610612766,
    "bulls": 1610612741,
    "cavaliers": 1610612739,
    "mavericks": 1610612742,
    "nuggets": 1610612743,
    "pistons": 1610612765,
    "warriors": 1610612744,
    "rockets": 1610612745,
    "pacers": 1610612754,
    "clippers": 1610612746,
    "lakers": 1610612747,
    "grizzlies": 1610612763,
    "heat": 1610612748,
    "bucks": 1610612749,
    "timberwolves": 1610612750,
    "pelicans": 1610612740,
    "knicks": 1610612752,
    "thunder": 1610612760,
    "magic": 1610612753,
    "76ers": 1610612755,
    "suns": 1610612756,
    "trail blazers": 1610612757,
    "kings": 1610612758,
    "spurs": 1610612759,
    "raptors": 1610612761,
    "jazz": 1610612762,
    "wizards": 1610612764,
}

# Needed because the API likes to give only the team ids
id_dict = {}


def _gen_id_dict():
    # Use name_dict to generate id_dict
    for k, v in name_dict.iteritems():
        id_dict[v] = k

# Generate id_dict when this module is imported
_gen_id_dict()


def get_id(name):
    """Returns the corresponding numeric id, given a team's short name.

    The numeric id will be returned as an integer.

    Raises a KeyError if the given team name is invalid.

    Keyword arguments:
    name -- the team's short name, such as "Lakers", case doesn't matter
    """
    return name_dict[name.lower()]


def get_name(id, capitalize=True):
    """Returns the corresponding team name, given a team's numeric id.

    The name is capitalized by default.

    Raises a KeyError if the given id is not recognized.

    Keyword arguments:
    id -- the team's numeric id as a string
    capitalize -- whether to capitalize the name (default: True)
    """
    result = id_dict[id]

    if not capitalize:
        return result
    else:
        # This is only here for the Trail Blazers, really
        # Also, go figure this line out yourself :p
        return " ".join([s.capitalize() for s in result.split()])


if __name__ == "__main__":
    # Test if I didn't typo the beginning of the id, since they all have
    # the same first 8 digits, and if the id is long enough
    for k, v in name_dict.iteritems():
        s = str(v)
        assert s.startswith("16106127") and len(s) == 10, k + " is bad id"

    print("id validity test passed")

    # Test get_id case insensitity
    assert get_id("Lakers") == name_dict["lakers"], "get_id failure"

    print("get_id test passed")

    # Test for duplicates
    for cur_team in name_dict.iterkeys():
        cur_id = name_dict[cur_team]

        for k, v in name_dict.iteritems():
            assert cur_team == k or cur_id != v, "duplicate found"

    print("duplicate check passed")

    # Test the symmetry of name_dict and id_dict
    for k, v in name_dict.iteritems():
        assert id_dict[v] == k
    for k, v in id_dict.iteritems():
        assert name_dict[v] == k

    print("symmetry check passed")

    # Test get_name with and without capitalization
    def test_name(name):
        return get_name(get_id(name))

    assert test_name("Lakers") == "Lakers", "get_name 1"
    assert test_name("Trail Blazers") == "Trail Blazers", "get_name 2"
    assert test_name("76ers") == "76ers", "get_name 3"
    assert get_name(get_id("Bulls"), False) == "bulls", "get_name lowercase"

    print("get_name checks passed")

