# silly-nba-predictions

I don’t watch basketball.

What I'm trying to do here is see if you can numerically predict winners of games.
My first trial was the 2016 season, which I had to unfortunately give up on because I left the country halfway through, and missed a lot of dates.

The 2017 season will be the first day by day trial of the method. At the end I'll plot the Elo rating of each team, as well as predictions vs true wins to try and refine the model.

At present the model is currently a base Elo model, with starting Elo 1500, and k factor 30.

## What is the Elo rating system?
For an in depth discussion look here: https://en.wikipedia.org/wiki/Elo_rating_system

In a non-rigorous mathematical explanation, the Elo system tries to predict who will win a game based on the Elo rating of two teams. It does not explicitly state that one team will win, rather it gives a *probability* that each team wins the game at hand. This probability is dependent on the Elo difference between the two teams.

After a game is played a team's Elo rating is updated according to the result. The k factor determines the maximum "Elo points" in play, in this case, 30. A team with a high probability to win who wins will gain very few Elo points and similarly for losers. A team predicted to lose will gain more points if they win, and a team predicted to win will lose more points if they lose.

The change in elo is always equal to P(winner)*k where P(winner) is the probability of winning for either team. It is max at ~k (if one team is so far ahead their probability of winning is close to 1), and is minimum at k/2 (where the teams are evenly matched and P(win) = 50%). 

Can we build a machine to predict basketball games based on pure math? We shall see.