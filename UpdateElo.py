import os
import csv
import math
import time
from operator import itemgetter
from Predict import predict,predict_set
from API import get_games
from datetime import date, timedelta



# K factor for elo changes.
kfactor = 30

path = os.getcwd()

#results = glob.glob(os.path.join('files',"*.txt"))

# Takes the elo file and loads it into a list.
# We'll need any non-changed Elos later.
# Then archives the old ratings to a new file designated by date.
def get_elo(date):
    
    elodate = date.strftime('%m-%d-%Y')
    elos = []

    fin = open(os.path.join('files','elo.txt'))

    fout = open(os.path.join('files',('elo-' + elodate + '.txt')),'w')

    # Reads in the lines in the elo file.
    for line in fin:
        # Writes the line to the new file.
        fout.write(line)
        
        line = line.rstrip('\n')  # Strips the newline

        # Splits by tab character, then removes all empty strings.
        # This leaves just the team and its elo value.
        team = line.split('\t')
        team = filter(None, team)

        # Convert the elo to a float.
        team[1] = float(team[1])

        elos.append(team)

    # Close the file readers.
    fin.close()
    fout.close()

    return elos

# Gets the game results from the results file.
def get_results_file():

    results = []

    # Opens the results csv as a csv.
    fin = open(os.path.join('data','results-2016-17.csv'))
    reader = csv.reader(fin)

    for line in reader:
        # This skips blank lines I think.
        if line:
            results.append(line)
        #print line

    fin.close()

    return results

# Gets the game results from the NBA.com API for games on date.
# Date should be a datetime object.
def get_results_API(date):

    NBAgames = get_games(date)
    games = []

    # For each NBAGame object, we'll need to make it a list.
    for game in NBAgames:
        tempgame = [0,0,0,0]
        tempgame[0] = game.home_team
        tempgame[1] = game.home_score
        tempgame[2] = game.away_team
        tempgame[3] = game.away_score
        games.append(tempgame)

    return games


# Gets the predicted result for a single game.
# Uses the given elos to make the prediction, and uses the result to determind
# the team.
def get_prediction(elos, result):

    singleelo = [0,0,0,0]
    for elo in elos:
        # If the first team is matched, make sure it's the beginning of the
        # list.
        if result[0] == elo[0]:
            singleelo[0] = elo[0]
            singleelo[1] = elo[1]
        elif result[2] == elo[0]:
            singleelo[2] = elo[0]
            singleelo[3] = elo[1]

    return predict(singleelo)

# For this  update Team 1 is the team occupying positions [0,1] in any list.
# Team 2 is positions [2,3].
# Note to self 10-17-17: use tuples????
# In all cases this is [teamname, value] where value is score, prediction, or elo.
# If this fails, the failure is likely somewhere else assigning a value wrong.
def update(elos, games):

    # Lists are objects so to avoid changing the original this copies it.
    new = list(elos)
    
    # We'll use the results array as the basis for our updates.
    for game in games:
        # For this loop, team1 and team2 are the elos of team1 and team2
        # If the team is found set its place in the temp elo array.
        for team in new:
            if game[0] == team[0]:
                team1 = team[1]
            elif game[2] == team[0]:
                team2 = team[1]

        # Gets the predicted result of the game.
        # This is required because the delta-elo is dependent on the prediction.
        # Predict using new elos so when you change them (in this loop) it uses
        # the updated values.
        predict = get_prediction(new, game)

        # This is for the elo change equation
        # Team status: 1 = team1 wins, 0 = team2 wins
        winner = 0
        # They might not be ints but they should be...
        if int(game[1]) > int(game[3]):
            winner = 1

        team1, team2 = elo_update([team1, team2], [predict[1], predict[3]], winner)

        # Puts the new elos into the elos array.
        # Run through the new elos array, if the team is matched to the change
        # then set its elo to the new changed elo.
        for team in new:
            if game[0] == team[0]:
                team[1] = team1
            elif game[2] == team[0]:
                team[1] = team2

    # Sorts the elos by highest to lowest
    new.sort(key=itemgetter(1), reverse=True)

    return new

# Updates the elo based on the elo formula.
# At present this is a standard elo formula.
# Assumes team[0/1] and predict[0/1] are linked.
# Team status: 1 = team1 wins, 0 = team2 wins
def elo_update(teams, predicts, winner):
    team1 = float(teams[0]) + kfactor * (winner - float(predicts[0]))
    team2 = float(teams[1]) + kfactor * ((1 - winner) - float(predicts[1]))
    
    return (team1, team2)

# Writes the elos to a new elo file.
def write(elos):

    fout = open(os.path.join('files','elo.txt'),'w')
    for team in elos:
        # Tab spacing aligns elos for different team name lengths
        if len(str(team[0])) >= 8:
            fout.write(str(team[0]) + '\t' + str(team[1]) + '\n')
        else:
            fout.write(str(team[0]) + '\t\t' + str(team[1]) + '\n')

    fout.close()

if __name__ == "__main__":
    updateday = date.today() - timedelta(1)
    write(update(get_elo(updateday), get_results_API(updateday)))
    print("Elo updated for " + updateday.strftime('%m-%d-%Y'))
