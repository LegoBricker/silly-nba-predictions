import os
import glob
import math


path = os.getcwd()
#print glob.glob(os.path.join('Results',"*.txt"))


# Returns an info list of the form [elos, games]
def load_info():

    # Lists for returning.
    elos = []
    games = []
    info = []

    # Find those required txt files
    results = glob.glob(os.path.join('files',"*.txt"))

    # Lets read these files.
    for file in results:
        # Get the name from the end of the filename split, then cut off that txt
        filename = file.split('/')[-1].split('.txt')[0]

        fin = open(file)
        # Takes the elo file and loads it into a list.
        if filename == "elo":
            for line in fin:
                line = line.rstrip('\n')  # Strips the newline

                # Split by tabs and strip out the empty elements
                team = line.split('\t')
                team = filter(None, team)

                elos.append(team)

        # Takes the upcoming games file and loads it into a list
        if filename == "upcoming":
            for line in fin:
                line = line.rstrip('\n')  # Strips the newline
                # Lines are in format team1-team2
                gameteams = line.split('-')

                games.append(gameteams)

    #TODO Tuple?
    info.append(elos)
    info.append(games)
    return info

# Takes in the list and returns a list of games.
# Each "game" is the two teams and their elo ratings.
def get_team_elos(gameinfo):
    elos = gameinfo[0]
    games = gameinfo[1]

    teamelos = []

    for game in games:
        singleelo = [0,0,0,0]

        # For each game lets find the team's current ratings.
        for team in elos:
            if team[0] == game[0]:
                singleelo[0] = team[0]
                singleelo[1] = float(team[1])
            elif team[0] == game[1]:
                singleelo[2] = team[0]
                singleelo[3] = float(team[1])

        teamelos.append(singleelo)

    # Returns the inputted (elo,game) tuple as a list of lists.
    # Inner lists are [team1, elo1, team2, elo2]
    return teamelos

# Predicts a set of games
# Team elos is a list of games to predict.
def predict_set(teamelos):
    predictions = []

    for game in teamelos:

        teampredictions = predict(game)
        predictions.append(teampredictions)

    return predictions

# Predicts a single game
def predict(game):

    gameprediction = [0,0,0,0]
    # Does the elo math to make the prediction, then appends it to the predictions list.
    gameprediction[1] = 1 / (1 + math.pow(10,(game[3] - game[1]) / 400))
    gameprediction[3] = 1 - gameprediction[1]
    gameprediction[0] = game[0]
    gameprediction[2] = game[2]

    return gameprediction


# towrite is a list of lists of the form [team1, predict1, team2, predict2]
# Same format that predict() returns games.
def write_predictions(towrite):
    # Loads up the prediction file for writing.
    fout = open(os.path.join('files', 'prediction.txt'), 'w')

    for game in towrite:
        # Prints the predictions to the file.
        fout.write(game[0] + ":" + str(game[1]) + '\n')
        fout.write(game[2] + ":" + str(game[3]) + '\n\n')

    fout.close()

if __name__ == "__main__":
    tempinfo = load_info()
    prediction = predict_set(get_team_elos(tempinfo))
    write_predictions(prediction)