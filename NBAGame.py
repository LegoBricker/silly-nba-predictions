#!/usr/bin/env python

from __future__ import print_function, division


class NBAGame:
    """A NBA game.

    Contains basic information about a specific NBA game, including
    whether it finished, the date it occurred, the names of the
    teams that participated, and the score.

    Note that its methods are only valid if the game is finished,
    which is the case if the 'done' field is True.

    Public instance variables:
    date, done, home_team, away_team, home_score, away_score

    Public methods:
    get_winner -- obtain the name of the team that won
    get_victory_margin -- obtain the margin of victory
    """
    def __init__(self, date, done, home_team, away_team,
                 home_score, away_score):
        """Create an NBAGame object.

        Keyword arguments:
        date -- a datetime.date object representing the date of the game
        done -- whether the game has concluded
        home_team -- name of the home team
        away_team -- name of the away team
        home_score -- number of points scored by the home team
        away_score -- number of points scored by the away team
        """
        self.date = date
        self.done = done
        self.home_team = home_team
        self.away_team = away_team
        self.home_score = home_score
        self.away_score = away_score

    def get_winner(self):
        """Retrieve the winning team's name.

        If the game is not done yet, this throws an exception, so
        check the 'done' field first.
        """
        if not self.done:
            raise Exception("The game isn't done yet")

        if self.home_score > self.away_score:
            return self.home_team
        else:
            return self.away_team

    def get_victory_margin(self):
        """Retrieve the margin of victory.

        If the game is not done yet, this throws an exception, so
        check the 'done' field first.
        """
        if not self.done:
            raise Exception("The game isn't done yet")

        if self.home_score > self.away_score:
            return self.home_score - self.away_score
        else:
            return self.away_score - self.home_score

    def __str__(self):
        """Return a string representation of this NBAGame object."""
        return (str(self.done) + " " + self.home_team + " " + self.away_team +
                " " + str(self.home_score) + " " + str(self.away_score))

