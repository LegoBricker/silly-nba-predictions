#!/usr/bin/env python

import argparse
from datetime import datetime, timedelta
import time

import API

def write_results(start, end, filename, interval=4):
    single_day = timedelta(days=1)

    cur_date = datetime(start.year, start.month, start.day)

    # Too lazy to use the csv module
    with open(filename, 'w') as fout:
        while cur_date <= end:
            cur_games = API.get_games(cur_date.date())

            print("Retrieved " + str(len(cur_games)) +
                  " games on " + str(cur_date))

            cur_date += single_day

            for game in cur_games:
                fout.write(game.home_team + "," + str(game.home_score) + "," +
                           game.away_team + "," + str(game.away_score) + "\n")

            time.sleep(interval)

    print("Results written.")

if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument("start", help="yyyy-mm-dd")
    parser.add_argument("end", help="yyyy-mm-dd")

    args = parser.parse_args()

    date_format = "%Y-%m-%d"
    start = datetime.strptime(args.start, date_format)
    end = datetime.strptime(args.end, date_format)

    if start > end:
        print("Start date must be before end date.")
    else:
        write_results(start, end, "results.csv")

