#!/usr/bin/env python

# Usually, I don't put too many comments, but I don't mind annotating
# my code more than usual. It'll help you learn stuff :p

# Import Python 3 features (aka the future):
# - print() is mandatory and print "Hello world" is an error
# - division is floating point by default, use // for integer division
from __future__ import print_function, division

# Module for parsing command line arguments
import argparse

# Module for working with dates and times
import datetime

# Module for encoding and decoding JSON
import json

# Provides methods for opening URLs as well as utility functions
# For legacy reasons that are cleaned up in Python 3, there are
# two libraries *sigh*
import urllib
import urllib2

# Module for working with zip-compressed data (includes gzip)
import zlib

# Import our own code here
from NBAGame import NBAGame
import Teams


def get_games(date):
    """Retrieve a list of games on the specified day.

    The list will contain NBAGame objects.

    date -- a datetime.date object representing the desired date
    """
    params = [('GameDate', date.strftime("%m/%d/%Y")),
              ('LeagueID', '00'),
              ('DayOffset', '0')]

    data = send_request("scoreboardV2", params)
    if data is None:
        return None

    result_sets = data["resultSets"]  # The data itself
    game_header = result_sets[0]  # Holds the list of games

    if game_header["name"] != "GameHeader":
        raise Exception("result_sets[0] is not GameHeader")

    # Figure out the indices for the items we want in the list of games
    game_item_names = game_header["headers"]  # List showing item order
    status_idx = game_item_names.index("GAME_STATUS_TEXT")
    home_id_idx = game_item_names.index("HOME_TEAM_ID")
    away_id_idx = game_item_names.index("VISITOR_TEAM_ID")

    line_score_header = result_sets[1]  # Holds the list of teams that played

    if line_score_header["name"] != "LineScore":
        raise Exception("result_sets[1] is not LineScore")

    score_item_names = line_score_header["headers"]
    team_id_idx = score_item_names.index("TEAM_ID")
    pts_idx = score_item_names.index("PTS")

    game_rowset = game_header["rowSet"]  # The list of games
    line_score_rowset = line_score_header["rowSet"]  # The list of teams

    games = []  # Return value

    # Iterate through the list of games and extract the needed info
    for entry in game_rowset:
        done = (entry[status_idx] == "Final")
        home_id = entry[home_id_idx]
        away_id = entry[away_id_idx]
        home_pts = None
        away_pts = None

        # Skip games with unconventional teams, such as those in
        # All-Star Weekends or certain preseason games
        if home_id not in Teams.id_dict or away_id not in Teams.id_dict:
            continue

        # Use the obtained team ids to find the number of points scored
        for team in line_score_rowset:
            if team[team_id_idx] == home_id:
                home_pts = team[pts_idx]
            if team[team_id_idx] == away_id:
                away_pts = team[pts_idx]

        if ((home_pts is not None and away_pts is None) or
                (away_pts is None and home_pts is not None)):
            raise Exception("Exactly one team doesn't have a score")

        # For games in the future, the point value is None, so set it
        # to zero for now
        if home_pts is None:
            home_pts = 0
        if away_pts is None:
            away_pts = 0

        # Get the actual names of the teams using the team ids
        home_name = Teams.get_name(home_id)
        away_name = Teams.get_name(away_id)

        # Create an NBAGame object and add it to the list to be returned
        games.append(NBAGame(date, done, home_name, away_name,
                             home_pts, away_pts))

    return games


def send_request(endpoint, params):
    """Send a request to stats.nba.com and returns the result.

    The return value is None if the request is unsuccessful.

    For advanced usage. It's probably best to ask Jjp137 to implement
    what you need instead :p

    Keyword arguments:
    endpoint -- string representing the part of the API to contact,
                such as "scoreboard"
    params -- a 2-tuple of keys and values to send to the server
    """
    # Create a string representing the parameters and tack them
    # on to the end of the URL (you know, those arguments separated by &
    # that are often seen at the end of URLs)
    base_url = "http://stats.nba.com/stats/" + endpoint + "/?"

    param_str = ""
    for key, value in params:  # Since it's a 2-tuple
        param_str += key + "=" + value + "&"

    param_str = param_str.rstrip('&')  # Take out the & at the end
    url = base_url + param_str

    # We need to set some silly headers because otherwise the server will
    # reject our request; in particular, the Referer is expected to be
    # a particular URL, we'll pretend to be a web browser, we'll request
    # that the data be sent as gzip-compressed data, and we'll also
    # add an Accept-Language header; this is all done to trick the server
    # into accepting our request
    req = urllib2.Request(url)
    req.add_header("Origin", "http://stats.nba.com")
    req.add_header("User-Agent", "Mozilla/5.0 (Windows NT 6.1; Win64; x64) "
                                 "AppleWebKit/537.36 (KHTML, like Gecko) "
                                 "Chrome/61.0.3163.100 Safari/537.36")
    req.add_header("Accept-Encoding", "gzip, deflate, sdch")
    req.add_header("Accept-Language", "en")

    try:
        # Send the HTTP request and get the results
        # Let's put a 15-second timeout, too
        result = urllib2.urlopen(req, None, 15)

        # If the request was successful, return the JSON data as a
        # Python data structure
        if result.getcode() == 200:
            # The data is gzip-compressed, so we have to decompress it first
            # before reading it as JSON; we use 47 here as the wbits here
            # to tell zlib that it's either zlib or gzip using the highest
            # window size possible
            data = zlib.decompress(result.read(), 47)

            return json.loads(data)  # loads() = strings, load() = objects
        else:
            return None

    except urllib2.URLError:
        raise  # FIXME: handle it more robustly later


# See: https://docs.python.org/3/library/__main__.html
# Basically, if this is launched from the command line, __name__ is
# set to '__main__' and this is often used for putting in test code
# that doesn't get executed if the module is imported instead
if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument("year", type=int)
    parser.add_argument("month", type=int)
    parser.add_argument("day", type=int)

    args = parser.parse_args()

    d = datetime.date(args.year, args.month, args.day)
    games = get_games(d)

    if not games:
        print("No games scheduled on that day.")

    for game in games:
        print(game)

